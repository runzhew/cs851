#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <limits.h>
#include <float.h>
#include <math.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include "Practical.h"

static const unsigned int TIMEOUT_SECS = 2; // Seconds between retransmits

unsigned long sequence = 0;
unsigned long missed = 0;
bool run = true;
bool done = false;

void sig_handler(int ignored); // Handler for SIGALRM
double getTime();
void printCurTime(char *tmpLine);

int main(int argc, char *argv[])
{

    if (argc != 7) // Test for correct number of arguments
        DieWithUserMessage("Parameter(s)",
                           "<Server Address/Name> <Port Number> <Delay> <Data size> <Burst Size> <Test Duration>\n");

    char *server = argv[1];     // First arg: server address/name
    char *service = argv[2];
    ssize_t numBytes = 0;
    unsigned int loss_in_burst = 0;
	double loss_rate = 0;
    double time = 0.0;
    double total_time = 0.0;
    double tmin = DBL_MAX;
    double tmax = 0;
    double tsum = 0;
    double tsum_dev = 0;
    double delay = strtod(argv[3], NULL);
    size_t datasize = (size_t)atol(argv[4]);
    if ((atol(argv[4]) < 0) || (datasize > MAXDATASIZE))
        DieWithUserMessage("Datasize is too big", "No more than 65507 Bytes (that means 64K - 28bytes)");

    int  burst_size = atoi(argv[5]);
    if (burst_size > 1000 || burst_size < 1)
        DieWithUserMessage("Burst Size is from 1 to 1000", "");
	double buffer[burst_size];

    int  duration = atoi(argv[6]);
    if (duration < 0 || duration > 65535)
        DieWithUserMessage("Duration is from 0 to 65535", "");

    char sendstring[datasize];
    char recvstring[datasize];

    delay = delay * 1000 * 1000; // for the use of usleep() function.
    total_time = getTime();

    printf("Sending packages to %s\n", argv[1]);

    // Tell the system what kind(s) of address info we want
    struct addrinfo addrCriteria;                   // Criteria for address
    memset(&addrCriteria, 0, sizeof(addrCriteria)); // Zero out structure
    addrCriteria.ai_family = AF_UNSPEC;             // Any address family
    addrCriteria.ai_socktype = SOCK_DGRAM;          // Only datagram sockets
    addrCriteria.ai_protocol = IPPROTO_UDP;         // Only UDP protocol

    // Get address(es)
    struct addrinfo *servAddr; // Holder for returned list of server addrs
    int rtnVal = getaddrinfo(server, service, &addrCriteria, &servAddr);
    if (rtnVal != 0)
        DieWithUserMessage("getaddrinfo() failed", gai_strerror(rtnVal));

    // Create a reliable, stream socket using TCP
    int sock = socket(servAddr->ai_family, servAddr->ai_socktype,
                      servAddr->ai_protocol); // Socket descriptor for client
    if (sock < 0)
        DieWithSystemMessage("socket() failed");

    // Set signal handler for alarm signal
    struct sigaction handler; // Signal handler
    handler.sa_handler = sig_handler;
    if (sigfillset(&handler.sa_mask) < 0) // Block everything in handler
        DieWithSystemMessage("sigfillset() failed");
    handler.sa_flags = 0;

    if (sigaction(SIGALRM, &handler, 0) < 0)
        DieWithSystemMessage("sigaction() failed for SIGALRM");
    if (sigaction(SIGINT, &handler, 0) < 0)
        DieWithSystemMessage("sigaction() failed for SIGINT");

    struct sockaddr_storage fromAddr; // Source address of server
    // Set length of from address structure (in-out parameter)
    socklen_t fromAddrLen = sizeof(fromAddr);

    while(run) {
		if (getTime() - total_time > 300)
			break;

        usleep(delay);
        int iterator_burst = 0;
        time = getTime();
		done = false;
       
	   	for (; iterator_burst <= burst_size; iterator_burst++) {
            memset(sendstring, 0, datasize);
            sprintf(sendstring, "%d", iterator_burst);

            numBytes = sendto(sock, sendstring, datasize, 0,
                              servAddr->ai_addr, servAddr->ai_addrlen);
			buffer[iterator_burst] = time;
			sequence += burst_size;

            if (numBytes < 0)
                DieWithSystemMessage("sendto() failed");
            else if (numBytes != datasize)
                DieWithUserMessage("sendto() error", "sent unexpected number of bytes");
		}
		//printf("send finished\n");

        alarm(TIMEOUT_SECS); // Set the timeout

		loss_in_burst = 0;
        while (true) {
			numBytes = recvfrom(sock, recvstring, datasize, 0,
                                (struct sockaddr *) &fromAddr, &fromAddrLen);

			if (done == false) {
				time = getTime() - buffer[strtoul(recvstring, NULL, 10)];
				//printf("time = %3.6f\n", time);
				loss_in_burst++;

                if (tmin > time)
                    tmin = time;
                if (tmax < time)
                    tmax = time;
                tsum += time;
                tsum_dev += time * time;
			}
			else {
				break;
			}
		}
		loss_in_burst = burst_size - loss_in_burst + 1;
		//printf("loss_in_burst = %d,  burst_size = %d\n", loss_in_burst, burst_size);
	    //printf("Loss rate in this burst is  %f%%\n", ((double)loss_in_burst / (double)burst_size) * 100);
		loss_rate += ((double)loss_in_burst / burst_size);
		missed = missed + loss_in_burst;

        alarm(0);
    }

    total_time = getTime() - total_time;

    printf("\n---%s statistics---\n", argv[1]);
    printf("%lu packets transmitted, %lu received, %3.6f%% lossed, \noverall burst loss rate %3.6f%%  time %3.6f seconds.\n",
           sequence, (sequence - missed), ((double)missed / sequence) * 100, loss_rate * 100, total_time);
    // calculate the avg time
    tsum = tsum / (sequence - missed);
    tsum_dev = tsum_dev / (sequence - missed);
    tsum_dev = sqrt(tsum_dev - tsum * tsum);

    printf("rtt min/avg/max/mdev = %3.6f/%3.6f/%3.6f/%3.6f seconds\n\n", tmin, tsum, tmax, tsum_dev);

    close(sock);
    exit(0);
}

// Handler for SIGALRM
void sig_handler(int ignored)
{
    switch(ignored) {
    case SIGALRM:
		done = true;
        break;
    case SIGINT:
        run = false;
    }
}

double getTime()
{
    struct timeval curTime;
    (void) gettimeofday (&curTime, (struct timezone *) NULL);
    return (((((double) curTime.tv_sec) * 1000000.0)
             + (double) curTime.tv_usec) / 1000000.0);
}

void printCurTime(char *tmpLine)
{
    struct timeval curTime;
    (void) gettimeofday(&curTime, (struct timezone *)0);
    sprintf(tmpLine,"%s ",  (char *)ctime((const time_t *)&(curTime.tv_sec)));
    //remove the CR that ctime adds
    tmpLine[strlen(tmpLine) - 2] = ' ';
    tmpLine[strlen(tmpLine) - 1] = '\0';
}


