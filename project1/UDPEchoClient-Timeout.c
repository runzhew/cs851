#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <limits.h>
#include <float.h>
#include <math.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include "Practical.h"

static const unsigned int TIMEOUT_SECS = 2; // Seconds between retransmits

unsigned long sequence = 0;
unsigned long missed = 0;
bool run = true;

void sig_handler(int ignored); // Handler for SIGALRM
double getTime();
void printCurTime(char *tmpLine);

int main(int argc, char *argv[]) {

	if (argc != 5) // Test for correct number of arguments
		DieWithUserMessage("Parameter(s)",
			"<Server Address/Name> <Port Number> <Delay> <Data size>\n");

	char *server = argv[1];     // First arg: server address/name
	char *service = argv[2];
	ssize_t numBytes = 0;
	double time = 0.0;
	double total_time = 0.0;
	char output[128];
	double tmin = DBL_MAX;
	double tmax = 0;
	double tsum = 0;
	double tsum_dev = 0;
	double delay = strtod(argv[3], NULL);
	size_t datasize = (size_t)atol(argv[4]);

	if ((atol(argv[4]) < 0) || (datasize > MAXDATASIZE))
		DieWithUserMessage("Datasize is too big", "No more than 65507 Bytes (that means 64K - 28bytes)");

	char sendstring[datasize];
	char recvstring[datasize];

	delay = delay * 1000 * 1000; // for the use of usleep() function.
	total_time = getTime();

	printf("Sending packages to %s\n", argv[1]);

	// Tell the system what kind(s) of address info we want
	struct addrinfo addrCriteria;                   // Criteria for address
	memset(&addrCriteria, 0, sizeof(addrCriteria)); // Zero out structure
	addrCriteria.ai_family = AF_UNSPEC;             // Any address family
	addrCriteria.ai_socktype = SOCK_DGRAM;          // Only datagram sockets
	addrCriteria.ai_protocol = IPPROTO_UDP;         // Only UDP protocol

	// Get address(es)
	struct addrinfo *servAddr; // Holder for returned list of server addrs
	int rtnVal = getaddrinfo(server, service, &addrCriteria, &servAddr);
	if (rtnVal != 0)
		DieWithUserMessage("getaddrinfo() failed", gai_strerror(rtnVal));

	// Create a reliable, stream socket using TCP
	int sock = socket(servAddr->ai_family, servAddr->ai_socktype,
			servAddr->ai_protocol); // Socket descriptor for client
	if (sock < 0)
		DieWithSystemMessage("socket() failed");

	// Set signal handler for alarm signal
	struct sigaction handler; // Signal handler
	handler.sa_handler = sig_handler;
	if (sigfillset(&handler.sa_mask) < 0) // Block everything in handler
		DieWithSystemMessage("sigfillset() failed");
	handler.sa_flags = 0;

	if (sigaction(SIGALRM, &handler, 0) < 0)
		DieWithSystemMessage("sigaction() failed for SIGALRM");
	if (sigaction(SIGINT, &handler, 0) < 0)
		DieWithSystemMessage("sigaction() failed for SIGINT");

	struct sockaddr_storage fromAddr; // Source address of server
	// Set length of from address structure (in-out parameter)
	socklen_t fromAddrLen = sizeof(fromAddr);

	while(run) {
		usleep(delay);
		sequence++;
		memset(sendstring, 0, datasize);
		sprintf(sendstring, "%lu", sequence);

		time = getTime();
		numBytes = sendto(sock, sendstring, datasize, 0,
				servAddr->ai_addr, servAddr->ai_addrlen);

		if (numBytes < 0)
			DieWithSystemMessage("sendto() failed");
		else if (numBytes != datasize)
			DieWithUserMessage("sendto() error", "sent unexpected number of bytes");
		
		alarm(TIMEOUT_SECS); // Set the timeout
		
		numBytes = recvfrom(sock, recvstring, datasize, 0,
				    (struct sockaddr *) &fromAddr, &fromAddrLen);
		
		if (numBytes < 0) {
	      		
			printCurTime(output);
			printf("%s 0.000000 %lu ( loss )\n", output, sequence);
			missed++;
		}
		
		if (numBytes >= 0) {
			if (strtoul(recvstring, NULL, 10) >= sequence) {
				time = getTime() - time;
				
				if (tmin > time)
					tmin = time;
				if (tmax < time)
					tmax = time;
				tsum += time;
				tsum_dev += time * time;

				printCurTime(output);
				printf("%s %3.6f seconds %s\n", output, time, recvstring);
			}
		}

		alarm(0);
	}

	total_time = getTime() - total_time;

	printf("\n---%s statistics---\n", argv[1]);
	printf("%lu packets transmitted, %lu received, %lu packets lossed, time %3.6f seconds.\n",
	       sequence, (sequence - missed), missed, total_time);
	// calculate the avg time 
	tsum = tsum / (sequence - missed);
	tsum_dev = tsum_dev / (sequence - missed);
	tsum_dev = sqrt(tsum_dev - tsum * tsum);

	printf("rtt min/avg/max/mdev = %3.6f/%3.6f/%3.6f/%3.6f seconds\n\n", tmin, tsum, tmax, tsum_dev);

	close(sock);
	exit(0);
}

// Handler for SIGALRM
void sig_handler(int ignored) {
	switch(ignored) {
	case SIGALRM:
		break;
	case SIGINT:
		run = false;
	}
}

double getTime()
{
	struct timeval curTime;
	(void) gettimeofday (&curTime, (struct timezone *) NULL);
	return (((((double) curTime.tv_sec) * 1000000.0) 
             + (double) curTime.tv_usec) / 1000000.0); 
}

void printCurTime(char *tmpLine)
{
	struct timeval curTime;
	(void) gettimeofday(&curTime, (struct timezone *)0);
	sprintf(tmpLine,"%s ",  (char *)ctime((const time_t *)&(curTime.tv_sec)));
	//remove the CR that ctime adds
	tmpLine[strlen(tmpLine) - 2] = ' ';
	tmpLine[strlen(tmpLine) - 1] = '\0';
}


